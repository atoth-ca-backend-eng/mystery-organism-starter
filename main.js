// Returns a random DNA base
const returnRandBase = () => {
  const dnaBases = ['A', 'T', 'C', 'G']
  return dnaBases[Math.floor(Math.random() * 4)] 
}

// Returns a random single stand of DNA containing 15 bases
const mockUpStrand = () => {
  const newStrand = []
  for (let i = 0; i < 15; i++) {
    newStrand.push(returnRandBase())
  }
  return newStrand
}

// Defines object factory function
const pAequorFactory = (num, arr) => {
  return {
    specimenNum: num,
    dna: arr,

    // .mutate() method 'mutates' an object's own DNA by altering randomly a single base of the DNA chain
    mutate() {
      const selectedBaseIdx = Math.floor(Math.random()*this.dna.length);
      let selectedBase = this.dna[selectedBaseIdx];
      let randomBase;
      do {
        randomBase = returnRandBase();
      } while (randomBase === selectedBase);
      this.dna[selectedBaseIdx] = randomBase;
      return this.dna;
    },

    // .compareDNA() method compares an object's own DNA with another object's DNA and informs about the overlap ratio
    compareDNA(obj) {
      let numIdenticalBases = 0;
      this.dna.forEach((elem, idx) => {
        if(elem===obj.dna[idx]) {
          numIdenticalBases += 1;
        }
      });
      console.log(`Specimen ${this.specimenNum} and specimen ${obj.specimenNum} have ${numIdenticalBases/this.dna.length*100}% DNA in common.`);
    },

    // .willLikelySurvive() method determines if the object is likely to survive or not by evaluating if the DNA chain consists of more than 60% of 'G' or 'C' bases
    willLikelySurvive() {
      let numOfSelBase = 0;
      this.dna.forEach(elem => {
        if(elem === 'C' || elem === 'G') {
          numOfSelBase +=1;
        }
      });
      return numOfSelBase/this.dna.length >= 0.6 ? true : false;
    }
  }
};


// generating a population of 30 objects with DNA chains indicating high likelihood of survival
const pAequorPopulation = [];
let runIdx = 1;
while (pAequorPopulation.length<30) {
  const obj = pAequorFactory(runIdx, mockUpStrand());
  if(obj.willLikelySurvive()) {pAequorPopulation.push(obj);}
  runIdx+=1;
}
